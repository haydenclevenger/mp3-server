package edu.sandiego.comp375.jukebox;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;
import java.util.Arrays;
import java.io.IOException;
import java.lang.Exception;

/**
 * Class representing a client to our Jukebox.
 */
public class AudioClient {

//globals
private static Thread player;
private static int max_song_id;

	public static void main(String[] args) throws Exception {
		Scanner s = new Scanner(System.in);

		System.out.println("Client: Connecting to localhost (127.0.0.1) port 6666");
		Socket socket = null;
		try {
			socket = new Socket("127.0.0.1", 6666);
		}
		catch (Exception e) {
			System.out.println(e);
			System.exit(1);
		}

		//initializing input/output data types for socket
		DataOutputStream out = new DataOutputStream(socket.getOutputStream());
		BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());

		//retrieving max song ID from server
		getMaxSongID(socket, out, bis);

		while (true) {
			System.out.print(">> ");
			String[] command = s.nextLine().split("\\s+");
			if (command[0].equals("list")) {
				list(socket, out, bis);
			}
			else if (command[0].equals("info")) {
				info(socket, out, bis, command);
			}
			else if (command[0].equals("play")) {
				play(socket, out, bis, command);
			}
			else if (command[0].equals("stop")) {
				stop(socket, out, bis);
			}
			else if (command[0].equals("exit")) {
				exit(socket, out);
				break;
			}
			else {
				System.err.println("ERROR: unknown command");
			}
		}

		System.out.println("Client: Exiting");
		socket.close();
		bis.close();
		out.close();
	}

	/*
	LIST function
	@param socket Socket to communicate over
	*/
	private static void list(Socket socket, DataOutputStream out, BufferedInputStream bis) {
		if (socket.isConnected()) {
			try {
				//checking if song playing
				if(player != null) {
					stop(socket,out,bis);
				}
				//crafting message
				byte[] msg = {0,0,0};
				out.write(msg, 0, 3);
				//receive song data and print
				int msg_type = (int)bis.read();
				if(msg_type != 0) {
					System.out.println("LIST: incorrect response message type: " + msg_type);
					return;
				}
				int song_id = (int)bis.read();
				int bytes_expected = (int)bis.read();
				byte[] response = new byte[bytes_expected];
				bis.read(response, 0, bytes_expected);
				System.out.print(new String(response));
			}
			catch (IOException e) {
				System.out.println("IOExeption in list");
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
	}

	/*
	INFO function
	@param socket Socket to communicate over
	@param bis BufferedInputStream to read in from server
	@param out DataOutputStream to send to server
	@param command User entered String
	*/
	private static void info(Socket socket, DataOutputStream out, BufferedInputStream bis, String[] command) {
		if(!argsOK(command)) { return; }
		int song_id = Integer.parseInt(command[1]);
		if (socket.isConnected()) {
			try {
				//checking if song playing
				if(player != null) {
					stop(socket,out,bis);
				}
				//crafting message
				byte[] msg = {1,(byte)song_id,0};
				out.write(msg, 0, 3);
				//receive song data and print
				int msg_type = (int)bis.read();
				if(msg_type != 1) {
					System.out.println("INFO: incorrect response message number: " + msg_type);
					return;
				}
				int sid = (int)bis.read();
				int bytes_expected = (int)bis.read();
				byte[] response = new byte[bytes_expected];
				bis.read(response, 0, bytes_expected);
				System.out.print(new String(response));
			}
			catch (IOException e) {
				System.out.println("IOException in info");
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
	}

	/*
	PLAY function
	@param socket Socket to communicate over
	@param bis BufferedInputStream to read in from server
	@param out DataOutputStream to send to server
	@param command User entered command string
	@param command User entered String
	*/
	private static void play(Socket socket, DataOutputStream out, BufferedInputStream bis, String[] command) {
		if(!argsOK(command)) { return; }
		//checking if audio already playing
		//Not Correct
		//if song plays to end
		//no new song can be played
		if(player != null) {
			stop(socket,out,bis);
		}
		int songid = Integer.parseInt(command[1]);
		byte song_id = (byte)songid;
		if (socket.isConnected()) {
			try {
				byte[] msg = {2, song_id,0};
				out.write(msg, 0, 3);
			}
			catch (IOException e) {
				System.out.println("Write exception in play");
				return;
			}
			//receive song data and play
			try {
				BufferedInputStream in = new BufferedInputStream(socket.getInputStream(), 1024);
				player = new Thread(new AudioPlayerThread(in));
				player.start();
			}
			catch (Exception e) {
				System.out.println("Exception in play");
				if(player != null) {
					player.interrupt();
					player = null;
				}
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
	}

	/*
	STOP function
	@param socket Socket to communicate over
	@param out DataOutputStream to send to server
	@param bis BufferedInputStream to clear
	*/
	private static void stop(Socket socket, DataOutputStream out, BufferedInputStream bis) {
		if (socket.isConnected()) {
			try {
				//crafting message
				byte[] msg = {3,0,0};
				out.write(msg, 0, 3);
				//closing thread if open
				if(player != null) {
					player.interrupt();
					player = null;
				}
				//clearing buffer
				clearBuffer(bis);
			}
			catch (IOException e) {
				System.out.println("Write exception in stop");
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
	}

	/*
	EXIT function
	@param socket Socket to communicate over
	@param out DataOutputStream to send to server
	*/
	private static void exit(Socket socket, DataOutputStream out) {
		if (socket.isConnected()) {
			try {
				byte[] msg = {4,0,0};
				out.write(msg, 0, 3);
				if(player != null) {
					player.interrupt();
					player = null;
				}
			}
			catch (IOException e) {
				System.out.println("Write error in exit");
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
		System.out.println("Goodbye!");
	}

	/*
	 * Function to retrieve the maximum song ID from server
	 * @param socket Socket to communicate over
	 * @param out DataOutputStream to send data out on
	 * @param bis BufferedInputStream to read data in on
	 * @return the max song ID
	 */
	private static void getMaxSongID(Socket socket, DataOutputStream out, BufferedInputStream bis) {
		if(socket.isConnected()) {
			try {
				byte[] msg = {5,0,0}; //Max ID request: 5
				out.write(msg, 0, 3);
				//receive song data and save
				int msg_type = (int)bis.read();
				if(msg_type != 5) {
					System.out.println("MAX_ID: incorrect response message type: " + msg_type);
					return;
				}
				int song_id = (int)bis.read();
				int bytes_expected = (int)bis.read();
				if(bytes_expected > 1) {
					System.out.println("Too many bytes received in getMaxSongID");
				}

				max_song_id = (int)bis.read();
				if(max_song_id <= 0) {
					System.out.println("No songs available");
				}
			}
			catch (IOException e) {
				System.out.println("IOExeption in max song ID");
				return;
			}
		}
		else {
			System.out.println("Socket not connected\n");
		}
	}

	/*
	 * Function to check commands to info and play
	 * Checks to make sure song ID present and within range
	 * @param command User entered command
	 * @return true if args ok, false otherwise
	 */
	private static boolean argsOK(String[] command) {
		if(command.length != 2) {
			System.out.println("Wrong number of args");
			return false;
		}
		int songID = -1;
		try {
			songID = Integer.parseInt(command[1]);
		}
		catch (NumberFormatException e) {
			System.out.println("Song ID must be an number");
			return false;
		}
		if(songID >= max_song_id || songID < 0) {
				System.out.println("Song ID must be a positive integer less than " + max_song_id);
				return false;
		}
		return true;
	}

	/*
	 * Function to clear data out of buffer
	 * @param bis BufferedInputStream to clear
	 */
	private static void clearBuffer(BufferedInputStream bis) {
		//clearing data out of buffer
		try {
			while(bis.skip(bis.available()) > 0);
		}
		catch (IOException e) {
			System.out.println("IOException when clearing buffer");
		}
	}
}
