/*
 * File: jukebox-server.cpp
 *
 * Authors: Erik Locke elocke@sandiego.edu
 * 			Hayden Clevenger
 *
 * Example code to help get you starter with your Jukebox server.
 * 
 * 
 *
 *
 */

#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <iostream>

#define BACKLOG (10)
#define MAX_CLIENTS (1024)
#define BUFF_SIZE (1024)

using namespace std;

struct SMPHeader {
	uint8_t instr, sid, size;
	//variable sized data
}__attribute__((packed));
enum State { IDLE, PLAY, INFO, LIST, EXIT };

class Client {
  public:
	int socket;
	int sid;
	char buff[BUFF_SIZE];
	int buff_bytes_sent;
	int bytes_in_buff;
	State state;
	FILE *fp;
	Client (int); //constructor
};
//constructor code
Client::Client (int sock) {
	state = IDLE;
	socket = sock;
	sid = -1;
	buff_bytes_sent = 0;
	bytes_in_buff = 0;
	fp = NULL;
}

// forward declarations
int acceptConnection(int server_socket);
void setNonBlocking(int sock);
int filter(const struct dirent *ent);
int readMP3Files(char *dir);
void send_max_id(Client client);
void receive_request(int sock, vector<Client> & clients);
int execute_request(int sock, vector<Client> & clients);
void list(Client &client);
void info(Client &client);
void play(Client &client);
void exit(vector<Client> &clients);

//globals
vector<string> info_list;
vector<string> song_list;
string directory;

/*
 * The main function for the server.
 * You should refactor this to be smaller and add additional functionality as
 * needed.
 */
int main(int argc, char **argv) {
    if (argc < 3) {
        printf("Usage:\n%s <port> <filedir>\n", argv[0]);
        exit(0);
    }

	//creating array of pointers to Clients to handle multiple clients
	vector<Client> clients;

	//get song and info lists
	directory.assign(argv[2]);

    // Get the port number from the arguments.
    uint16_t port = (uint16_t) atoi(argv[1]);

    // Create the socket that we'll listen on.
    int serv_sock = socket(AF_INET, SOCK_STREAM, 0);

    /* 
	 * Set SO_REUSEADDR so that we don't waste time in TCP's TIME_WAIT when we
	 * shut down the connection.
	 */
	int val = 0; //to appease valgrind gods
    val = setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if (val < 0) { perror("setsockopt"); exit(1); }

    /* Set our server socket to non-blocking mode.  This way, if we
     * accidentally accept() when we shouldn't have, we won't block
     * indefinitely. */
    setNonBlocking(serv_sock);

    /* 
	 * Read the other argument (mp3 directory).
	 * See the notes for this function above.
	 */
    int song_count = readMP3Files(argv[2]);
    printf("Found %d songs.\n", song_count);

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    // Bind our socket and start listening for connections.
    val = ::bind(serv_sock, (struct sockaddr*)&addr, sizeof(addr));
    if(val < 0) {
        perror("bind");
        exit(1);
    }

	// Set our socket to listen for connections (which we'll later accept)
    val = listen(serv_sock, BACKLOG);
    if(val < 0) {
        perror("listen");
        exit(1);
    }

	//initializing socket file descriptor sets
    fd_set all_fds, read_fds, write_fds;
	FD_ZERO(&all_fds);
	FD_ZERO(&read_fds);
	FD_ZERO(&write_fds);
	FD_SET(serv_sock, &all_fds);

	int max_fd = serv_sock;

    while (1) {

		read_fds = all_fds;
		write_fds = all_fds;

		/* 
		 * select() will block until one of the sockets we asked about is ready for
		 * recving/sending.
		 * IMPORTANT NOTE: Select only checks up to, but not including, the
		 * value in the first param. For that reason, we'll add 1 to max_fd to
		 * ensure that max_fd actually gets checked.
		 */
        val = select(max_fd+1, &read_fds, &write_fds, NULL, NULL);

        if (val < 1) {
            perror("select");
            continue;
        }

        /* Check the FD_SETs after select returns.  These will tell us
         * which sockets are ready for recving/sending. */
        for (int i = 0; i <= max_fd; ++i) {
            if (FD_ISSET(i, &read_fds)) {
				if (i == serv_sock) {
					// If the server socket is ready for "reading," that implies
					// we have a new client that wants to connect so lets
					// accept it.
					int client_fd = acceptConnection(serv_sock);
					//add client to fd set and update max fd
					FD_SET(client_fd, &all_fds);
					if (client_fd > max_fd) max_fd = client_fd;
					printf("Accepted a new connection!\n");

					// Set this to non-blocking mode so we never get hung up
					// trying to send or receive from this client.
					setNonBlocking(client_fd);

					//create new Client and add it to array
					Client c = Client(client_fd);
					clients.push_back(c);					
				}
				else {
					// Socket descriptor i is ready for reading
					receive_request(i, clients);
				}
            }
            if (FD_ISSET(i, &write_fds)) {
                // Socket descriptor i is ready for writing.
				if(execute_request(i, clients) != 0) {
					FD_CLR(i, &all_fds);
				}
            }
        }
    }
}

/*
 * Recieves and parses client request
 * Will change client state to reflect request
 *
 * @param sock Socket descriptor 
 * @param clients Array of pointers to all clients
 */
void receive_request(int sock, vector<Client> & clients){
	//locate client
	size_t i;	//will be set to index of client
	for(i = 0; i < clients.size(); i++) {
		if(clients[i].socket == sock) {
			break;
		}
	}
	//read in data from client
	uint8_t buff[BUFF_SIZE];
	memset(buff, 0, sizeof(buff));
	int bytes = recv(sock, buff, BUFF_SIZE, 0);
	if(bytes <= 0) {
		perror("Recv Error: ");
		clients[i].state = EXIT;
		return;
	}
	//parse out headers
	uint8_t request_num = buff[0];
	uint8_t song_id = buff[1];

	switch(request_num) {
		case 0: //list
			clients[i].state = LIST;
			break;
		case 1: //info
			clients[i].state = INFO;
			clients[i].sid = song_id;
			break;
		case 2: //play
			clients[i].state = PLAY;
			clients[i].sid = song_id;
			clients[i].fp = fopen((directory + song_list[song_id]).c_str(), "rb");
			break;
		case 3: //stop
			if(clients[i].fp != NULL) {
				fclose(clients[i].fp);
				clients[i].fp = NULL;
			}
			clients[i].state = IDLE;
			break;
		case 4: //exit
			clients[i].state = EXIT;
			break;;
		case 5: //return max song ID
			send_max_id(clients[i]);
			clients[i].state = IDLE;
			break;
		default: //error
			printf("Bad request_num: %d\n", request_num);
			break;
	}	
}
/*
 * Executes which requests the client makes
 *
 * @param sock Socket descriptor
 * @param clients Array of pointers to Client structs
 * @return 1 if client is exiting, otherwise 0
 */
int execute_request(int sock, vector<Client> &clients){
	size_t i;	//will be set to index of client
	for(i = 0; i < clients.size(); i++) {
		if(clients[i].socket == sock) {
			break;
		}
	}
	switch(clients[i].state){
		case IDLE:
			return 0;
		case LIST:
			list(clients[i]);
			return 0;
		case INFO:
			info(clients[i]);
			return 0;
		case PLAY:
			play(clients[i]);
			return 0;
		case EXIT:
			exit(clients);
			return 1;
		default:	//should not reach here
			cout << "Client state corrupted! Closing Client" << endl;
			clients[i].state = EXIT;
	}
	return 0;
}

/*
 * Sends a response listing all the songs
 * @param client Client to send list to
 */
void list(Client &client){
	//hold final message
	uint8_t buff[BUFF_SIZE];
	memset(buff, 0, BUFF_SIZE);
	//header for protocol
	SMPHeader *hdr = (SMPHeader*)buff;
	hdr->instr = 0;
	hdr->sid = 0;
	//keep track of where to write each new song into buff
	int buff_ptr = 3;
	string song;
	for(size_t i = 0; i < song_list.size(); ++i){
		song = to_string(i);
		song += ". " + song_list[i] + "\n";
		strncpy((char *)buff+buff_ptr, (char *)song.c_str(), song.size());
		buff_ptr += song.size();
		song.clear();
	}
	hdr->size = buff_ptr;
	//send
	send(client.socket,buff, buff_ptr+3, 0);
	//update client state
	client.state = IDLE;
}

/*
 * Sends a response listing info for requested song
 * @param client Client to send info to
 */
void info(Client &client){
	//hold final message
	uint8_t buff[BUFF_SIZE];
	memset(buff, 0, BUFF_SIZE);
	//header for protocol
	SMPHeader *hdr = (SMPHeader*)buff;
	hdr->instr = 1;
	hdr->sid = 0;
	hdr->size = info_list[client.sid].size();
	//write in song info
	strncpy((char *)buff+3, (char *)info_list[client.sid].c_str(), 
		info_list[client.sid].size());
	//send
	send(client.socket,buff, info_list[client.sid].size()+3, 0);
	//update client state
	client.state = IDLE;
}

/*
 * Sends song data to be played at client end
 * @param client Client to send song bytes to
 */
void play(Client &client) {
	//write in song info
	if(client.bytes_in_buff == client.buff_bytes_sent) {
		client.bytes_in_buff = 0;	//reset for this new block of data
		client.buff_bytes_sent = 0;
		client.bytes_in_buff = fread(client.buff, sizeof(uint8_t), BUFF_SIZE, client.fp);
		client.buff_bytes_sent = send(client.socket, client.buff, client.bytes_in_buff, 0);
		if(client.buff_bytes_sent == -1) { //socket error
			printf("Client socket error. Closing socket\n");
			client.state = EXIT;
			return;
		}
		if(client.bytes_in_buff < BUFF_SIZE) {
			if(client.fp != NULL) {
				fclose(client.fp);
				client.fp = NULL;
			}
			client.state = IDLE;
		}
	}
	else {	//not all bytes sent on last iteration
		client.buff_bytes_sent = send(client.socket, client.buff + client.buff_bytes_sent, 
			BUFF_SIZE - client.bytes_in_buff, 0);
	}
}

/*
 * Function to remove client
 * @param clients Reference to a vector of clients
 */
void exit(vector<Client> &clients) {
	for(size_t i = 0; i < clients.size(); i++) {
		if(clients[i].state == EXIT) {
			close(clients[i].socket);
			if(clients[i].fp != NULL) {
				fclose(clients[i].fp);
				clients[i].fp = NULL;
			}
			clients.erase(clients.begin() + i);
			cout << "Client disconnected" << endl;
			break;
		}
	}
}

/*
 * Function to send the max song ID to a client
 * @param client Client to send to
 */
void send_max_id(Client client) {
	//hold final message
	uint8_t buff[BUFF_SIZE];
	memset(buff, 0, BUFF_SIZE);
	//header for protocol
	SMPHeader *hdr = (SMPHeader*)buff;
	hdr->instr = 5;
	hdr->sid = 0;
	hdr->size = 1;
	//write in song info
	uint8_t maxID = 0;
	for(size_t i = 0; i < song_list.size(); i++) {
		maxID++;
	}
	buff[3] = maxID;
	//send
	send(client.socket, buff, 4, 0);
}

/**
 * Accepts a connection and returns the socket descriptor of the new client
 * that has connected to us.
 *
 * @param server_socket Socket descriptor of the server (that is listening)
 * @return Socket descriptor for newly connected client.
 */
int acceptConnection(int server_socket) {
	struct sockaddr_storage their_addr;
	socklen_t addr_size = sizeof(their_addr);
	int new_fd = accept(server_socket, (struct sockaddr *)&their_addr,
			&addr_size);
	if (new_fd < 0) {
		perror("accept");
		exit(1);
	}

	return new_fd;
}

/* 
 * Use fcntl (file control) to set the given socket to non-blocking mode.
 *
 * @info Setting your sockets to non-blocking mode is not required, but it
 * might help with your debugging.  By setting each socket you get from
 * accept() to non-blocking, you can be sure that normally blocking calls like
 * send, recv, and accept will instead return an error condition and set errno
 * to EWOULDBLOCK/EAGAIN.  I would recommend that you set your sockets for
 * non-blocking and then explicitly check each call to send, recv, and accept
 * for this errno.  If you see it happening, you know that you're attempting
 * to call one of those functions when you shouldn't be.
 *
 * @param sock The file descriptor for the socket you want to make
 * 				non-blocking.
 */
void setNonBlocking(int sock) {
    /* Get the current flags. We want to add O_NONBLOCK to this set. */
    int socket_flags = fcntl(sock, F_GETFD);
    if (socket_flags < 0) {
        perror("fcntl");
        exit(1);
    }

    /* Add in the O_NONBLOCK flag by bitwise ORing it to the old flags. */
    socket_flags = socket_flags | O_NONBLOCK;

    /* Set the new flags, including O_NONBLOCK. */
    int result = fcntl(sock, F_SETFD, socket_flags);
    if (result < 0) {
        perror("fcntl");
        exit(1);
    }

    /* The socket is now in non-blocking mode. */
}

/* 
 * Checks wheter a filename ends in '.mp3'. 
 *
 * @info This is used below in readMP3Files. You probably don't need to use
 * it anywhere else.
 *
 * @param ent Directory entity that we are going to check.
 *
 * @return 0 if ent doesn't end in .mp3, 1 if it does.
 */
int filter(const struct dirent *ent) {
    int len = strlen(ent->d_name);

    return !strncasecmp(ent->d_name + len - 4, ".mp3", 4);
}

/*
 * Given a path to a directory, this function scans that directory (using the
 * handy scandir library function) to produce an alphabetized list of files
 * whose names end in ".mp3".  For each one, it then also checks for a
 * corresponding ".info" file and reads that in its entirety.
 *
 * @info You'll need to edit this to meet your needs (i.e. don't expect this
 * to do everything you want without any effort).
 *
 * @param dir String that represents the path to the directory that you want
 * 				to check.
 *
 * @return Number of MP3 files found inside of the specified directory.
 */
int readMP3Files(char *dir) {
    struct dirent **namelist;
    int i,n;

    n = scandir(dir, &namelist, filter, alphasort);
    if (n < 0) {
        perror("scandir");
        exit(1);
    }

    for (i = 0; i < n; ++i) {
        int bytes_read = 0;
        int total_read = 0;
        char path[1024];

        FILE *infofile = NULL;

        char *infostring = NULL;

        /* namelist[i]->d_name now contains the name of an mp3 file. */
        /* FIXME: You probably want to use this name to find file data. */
		string song_name(namelist[i]->d_name);
		song_list.push_back(song_name);
        printf("(%d) %s\n", i, namelist[i]->d_name);

        /* Build a path to a possible input file. */
        strcpy(path, dir);
        strcat(path, "/");
        strcat(path, namelist[i]->d_name);
        strcat(path, ".info");

        infofile = fopen(path, "r");
        if (infofile == NULL) {
            /* It wasn't there (or failed to open for some other reason). */
            infostring = (char*)"No information available.";
        } else {
            /* We found and opened the info file. */
            int infosize = 1024;
            infostring = (char*)malloc(infosize);

            do {
                infosize *= 2;
                infostring = (char*)realloc(infostring, infosize);

                bytes_read = fread(infostring + total_read, 1, infosize - total_read, infofile);
                total_read += bytes_read;
            } while (bytes_read > 0);

            fclose(infofile);

            /* Zero-out the unused space at the end of the buffer. */
            memset(infostring + total_read, 0, infosize - total_read);
        }

        /* infostring now contains the info data for this song. */
        /* FIXME: Use these info strings when clients send info commands. */
		string infocpp("Info:");
		string infostringcpp(infostring);
		infocpp = infocpp + infostringcpp + "\n";
		info_list.push_back(infocpp);
        printf("Info:%s\n\n", infostring);

        free(namelist[i]);
    }
    free(namelist);

    /* Return the number of files we found. */
    return n;
}
