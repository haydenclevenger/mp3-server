/**
 * This program will send a file to a client using non-blocking I/O.
 */
#define _XOPEN_SOURCE

#include <assert.h>
#include <inttypes.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>

#define BUFSIZE (128)
#define BACKLOG (10)
#define MAX_CLIENTS (42)

typedef struct {
    FILE *file;						// file being sent 
    int socket;						// socket descriptor used to send
    char buff[BUFSIZE];				// current chunk of file
    int bytes_in_buff;				// bytes in buffer
    int buff_bytes_sent;			// bytes used so far; <= bytes_in_buff
    enum { IDLE, SENDING } state;	// what we're doing
} FileSender;

/**
 * Use fcntl (file control) to set the given socket to non-blocking mode.
 *
 * @param sock The file descriptor for the socket you want to make
 * 				non-blocking.
 */
void setNonBlocking(int sock) {
	// Get the current flags. We want to add O_NONBLOCK to this set.
	int socket_flags = fcntl(sock, F_GETFD);
	if (socket_flags < 0) {
		perror("fcntl");
		exit(1);
	}

	socket_flags = socket_flags | O_NONBLOCK;

	// Set the new flags, including O_NONBLOCK.
	int result = fcntl(sock, F_SETFD, socket_flags);
	if (result < 0) {
		perror("fcntl");
		exit(1);
	}
}



/**
 * Initialize a sender to start sending a file on the given socket.
 * The socket must have been set to non-blocking mode.
 *
 * @param fs The FileSender that will do the sending.
 * @param filename The name of the file to send.
 * @param socket A socket descriptor to send data on.
 */
void init_sender(FileSender *fs, const char *filename, int socket) {
	FILE *file = fopen(filename, "r");
	if(file == NULL) {
		perror("error opening file");
		exit(1);
	}

	// Set initial state of our sender, indicating we're in sending state.
	fs->file = file;
	fs->socket = socket;
	fs->bytes_in_buff = 0;
	fs->buff_bytes_sent = 0;
	fs->state = SENDING;
}

/**
 * Continue sending the file started by init_sender().
 * Call this periodically.
 *
 * @param fs The FileSender we are using for this IO
 * @return nonzero when done.
 */
int send_data(FileSender *fs)
{
	// If we tried calling this but we're IDLE, return immediately, as
	// there is nothing to do
	if(fs->state == IDLE) {
		return 1;
	}

	// If buffer empty, fill it by reading from the file.
	// If we try to read but find we've reached the end of the file, then we
	// are done sending!
	if(fs->bytes_in_buff == fs->buff_bytes_sent) {
		fs->bytes_in_buff = 0;	//reset for this new block of data
		fs->buff_bytes_sent = 0;
		fs->bytes_in_buff = fread(fs->buff, sizeof(char), BUFSIZE, fs->file);
		fs->buff_bytes_sent = send(fs->socket, fs->buff, fs->bytes_in_buff, 0);
		if(fs->bytes_in_buff < BUFSIZE) {	//reached end of file
			close(fs->socket);
			fclose(fs->file);
			return 1;
		}
	}
	else {	//not all bytes sent on last iteration
		printf("not all bytes sent on last iteration!\n");
		printf("buff_bytes_sent: %d\n", fs->buff_bytes_sent);
		printf("bytes_in_buff: %d\n", fs->bytes_in_buff);
		fs->buff_bytes_sent += send(fs->socket, fs->buff + fs->buff_bytes_sent, BUFSIZE - fs->buff_bytes_sent, 0);
	}

	return 0;
}


int main(int argc, char **argv) {
	if (argc < 3) {
        printf("Usage:\t%s <port> <filename>\n", argv[0]);
        exit(0);
    }

	//create array of pointers to FileSenders to handle multiple clients
	FileSender *file_sndrs[MAX_CLIENTS] = {NULL};

    // Get the port number from the arguments.
    uint16_t port = (uint16_t) atoi(argv[1]);

	//get filename from arguments
	char *filename = argv[2];

    // Create the socket that we'll listen on.
    int serv_sock = socket(AF_INET, SOCK_STREAM, 0);

    // Set SO_REUSEADDR so that we don't waste time in TIME_WAIT.
    int val = 0;	//to appease valgrind gods
	val = setsockopt(serv_sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if (val < 0) {
        perror("Setting socket option failed");
        exit(1);
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    // Bind our socket and start listening for connections.
    val = bind(serv_sock, (struct sockaddr*)&addr, sizeof(addr));
    if(val < 0) {
        perror("Error binding to port");
        exit(1);
    }

    val = listen(serv_sock, BACKLOG);
    if(val < 0) {
        perror("Error listening for connections");
        exit(1);
    }

	//create FD sets
	fd_set all_fds, read_fds, write_fds;
	//clear out sets
	FD_ZERO(&all_fds);
	FD_ZERO(&read_fds);
	FD_ZERO(&write_fds);
	//add serv_sock to the all_fds set
	FD_SET(serv_sock, &all_fds);
	//max fd for checking sets
	int max_fd = serv_sock;

	while (1) {

		//in this example we are just writing to clients
		read_fds = all_fds;
		write_fds = all_fds;

		//block until one of our sockets is ready to receive data
		int sel_ret = select(max_fd+1, &read_fds, &write_fds, NULL, NULL);
		
		if(sel_ret < 1) {
			perror("select");
			continue;
		}

		//check the FD_SETs to see which clients need to be read from or written to
		for(int i = 0; i <= max_fd; i++) {
			//socket wants to be read from
			if(FD_ISSET(i, &read_fds)) {
				if(i == serv_sock) {
					// Accept a connection with the client
					struct sockaddr_storage their_addr;
					socklen_t addr_size = sizeof(their_addr);
					int client_fd = accept(serv_sock, (struct sockaddr *)&their_addr, &addr_size);
					if (client_fd < 0) {
						perror("accept");
						exit(1);
					}

					//add to all fds set
					FD_SET(client_fd, &all_fds);

					//update max fd
					max_fd = max_fd > client_fd ? max_fd : client_fd;

					//set the client's fd to non-blocking mode.
					setNonBlocking(client_fd);

					//Create a file sender and initialize it
					FileSender *fs = malloc(sizeof(FileSender));
					init_sender(fs, filename, client_fd);
					for(int j = 0; j < MAX_CLIENTS; j++) {
						if(file_sndrs[j] == NULL) {
							file_sndrs[j] = fs;
							break;
						}
					}
				}
			}
			//socket wants to be written to
			if(FD_ISSET(i, &write_fds)) {
				//send data
				for(int j = 0; j < MAX_CLIENTS; j++) {
					if(file_sndrs[j] != NULL) {
						if(file_sndrs[j]->socket == i) {
							if(send_data(file_sndrs[j]) != 0) {
								FD_CLR(i, &all_fds);
								free(file_sndrs[j]);
								file_sndrs[j] = NULL;
							}
							break;
						}
					}
				}
			}
		}
	}
}
