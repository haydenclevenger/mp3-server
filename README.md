Authors: 	Hayden Clevenger	-	hayden.clev@gmail.com
			Erik Locke
			Saturnino Garcia

Created: January 3, 2017

This repository includes a client and server program for 
requesting, providing and playing mp3 files.

This is a copy of a project completed with starter code 
from Saturnino Garcia and contributions from Erik Locke.

Server code requires Apache Maven build manager to compile.
